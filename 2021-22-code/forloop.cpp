#include <iostream>
#include <random>
using namespace std;
int main()
{


   const int nSimulations { 127 };

	for (int i { 33 }; i <= nSimulations; i++)
   { 

	   cout << " i "<< i<<" "<<" ";
      if (i % 13 == 0) cout << '\n';
   }   cout << '\n';
   
   
   for (int j = 0; j < 5; j++) {
  cout <<"\t\t"<< j << "\n";
}
   
   return 0;
}



/*
Notes:

    const char* is a pointer to a constant char
    char const* is a pointer to a constant char
    char* const is a constant pointer to a (mutable) char
*/
