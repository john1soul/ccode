// file: checkprimes.cpp
#include <iostream>
#include <cmath>
using namespace std;

bool isprime(long long int p){
	bool prime = true;
	long long int mff,divisor; //mff(middle first factor)
	//cout<<p<<" "<<mff<<endl;
	//long long int middlefactor = (long long int)longsqrt ;
	divisor = 3;
	while (divisor < mff && prime == true){
		if(p % divisor==0){
			prime = false;
		}
		divisor++;
	}
	//cout<<p<< " - "<<prime<<endl;
	return prime;
}

int main(){
long long int n;
int count = 1;
n = 3;
bool prime;
cout<<"NUMBER | PRIME COUNT | prime (0=false 1=true) \n";
cout<<"2 \t\t 1"<<endl;
	while (n < 70000){
		//cout <<n<<endl;
		prime = isprime(n);
		if (prime) {
			count++;
			cout<<"NUMBER | PRIME COUNT | prime (0=false 1=true)\n";
			cout<<n<<"\t"<<count<<"\t"<<prime<<endl;
		}
		n = n + 2;
		
	}
}

