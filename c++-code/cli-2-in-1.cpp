//xor
// https://kylewbanks.com/blog/Simple-XOR-Encryption-Decryption-in-Cpp
#include <bits/stdc++.h> 
#include <iostream>

using namespace std;

string encryptDecrypt(string toEncrypt,char key) {
    //char key = 'K'; //Any char will work
    string output = toEncrypt;
    
    for (int i = 0; i < toEncrypt.size(); i++)
        output[i] = toEncrypt[i] ^ key;
    
    return output;
}


int main(int argc, char** argv)
{
	// command line inputs are strings
	string inputstr = argv[1];
	string keystr = argv[2];
	// string to char
	char arr[keystr.length() + 1]; 
    strcpy(arr, keystr.c_str()); 
	char key = arr[0];
	//char key = 'k';
	cout<<inputstr<<" "<<key<<endl;
	string encrypted = encryptDecrypt(inputstr,key);
	cout<<"encrypted text >";
	cout<<encrypted;
	cout<<"\n\n";
	
	string dec = encrypted;
	//key = '0';
	string decrypted = encryptDecrypt(dec,key);
	cout<<"decrypted >";
	cout<<decrypted;
	cout<<"\n\n";
	
    return 0;
}
