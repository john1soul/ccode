directions
to run c code or c++ code in geany, you first need to write the code.
after you do that, click the build button on the top of the application.
It should look like a brick. After that, click on the Execute button, 
it looks like a set of 3 gears. Once you do that, the code should run!
