Directions for how to run C-C++ code:
If you cannot figure out how to run your code, pay close attention to what 
I'm about to show you. 

If it's C code (Example is Helloworld)

// compile ; gcc helloworld.c -o helloworld.o

*MAKE SURE TO INCLUDE <stdio.h> IN YOUR CODE*

If it's C++ code (Example is Helloworld)

// compile ; g++ helloworld.cpp -o hwcpp.o

*YOU DO NOT HAVE TO CHANGE IT TO hwcpp.o, YOU CAN LEAVE IT AS IS.*

To run your code, simply put in ./a.out in the command line. 
This will run your code in Terminal, or the CMD, or Command Line.
To run it in geany (Like a noob) look at my Geany directory.

If you want to compile it into a .o file, you would do 
g++ examplecode.cpp -o ec.o

This will output it onto your directory as ec.o, which you can run using 
./ec.o
