Sorting data is a Computer Science Fundamental.

Sorting is critical to many tasks. I will point out just a few, but they are characteristic of the kinds of things you use sorting for.

Example 1: Given a million data records, remove or merge the duplicates.

If the data records are sorted by the main uniqueness criterion, 
removing the duplicates can be done easily by traversing the list and finding groups of consecutive records that compare equal using that criterion.

Example 2: Compare two large sets of items and find out where they differ.

Example 3: Before data can be searched with a binary search the data will need to be sorted alphabetically or numberically.

There are many types of sorting algorithms.  Here are three.

Insertion sort is a simple sorting algorithm that works similar to the way you sort playing cards in your hands. 
The array is virtually split into a sorted and an unsorted part. 
Values from the unsorted part are picked and placed at the correct position in the sorted part.

The selection sort algorithm sorts an array by repeatedly finding the minimum element (considering ascending order) 
from unsorted part and putting it at the beginning. The algorithm maintains two subarrays in a given array.

The subarray which is already sorted. 
Remaining subarray which is unsorted.
In every iteration of selection sort, the minimum element (considering ascending order) from the unsorted subarray 
is picked and moved to the sorted subarray. 


CSort:
https://www.cplusplus.com/reference/algorithm/sort/
